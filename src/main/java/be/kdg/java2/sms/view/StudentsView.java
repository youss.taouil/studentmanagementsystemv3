package be.kdg.java2.sms.view;

import be.kdg.java2.sms.service.models.Student;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public interface StudentsView {
    TableView<Student> getTvStudents();

    TextField getTfName();

    DatePicker getDpBirth();

    TextField getTfLength();

    Button getBtnSave();
}
