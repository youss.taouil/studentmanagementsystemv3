package be.kdg.java2.sms.view.implementation;

import be.kdg.java2.sms.exceptions.StudentException;
import be.kdg.java2.sms.service.UserService;
import be.kdg.java2.sms.view.LoginView;
import be.kdg.java2.sms.view.Presenter;
import be.kdg.java2.sms.view.StudentsView;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class LoginPresenterImpl implements Presenter {
    private static final Logger L = Logger.getLogger(LoginPresenterImpl.class.getName());
    private final LoginView loginView;
    private final UserService userService;

    @Autowired
    public LoginPresenterImpl(LoginView loginView, UserService userService,
                              Scene scene, StudentsView studentsView) {
        this.loginView = loginView;
        this.userService = userService;
        addEventHandlers(scene, studentsView);
    }

    private void addEventHandlers(Scene scene, StudentsView studentsView) {
        loginView.getBtnAdd().setOnAction(e->{
            L.info("Add clicked...");
            try {
                userService.addUser(loginView.getTfName().getText(), loginView.getPfPassword().getText());
            } catch (StudentException se) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText("Cannot add user:");
                alert.setContentText(se.getMessage());
                alert.showAndWait();
            }
        });
        loginView.getBtnLogin().setOnAction(e->{
            L.info("Login clicked...");
            if (userService.login(loginView.getTfName().getText(),loginView.getPfPassword().getText())){
                L.info("Loggin succeeded!");
                scene.setRoot((Parent) studentsView);
                scene.getWindow().sizeToScene();
            } else {
                L.info("Loggin failed!");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText("Login failed");
                alert.setContentText("User does not exist or password wrong.");
                alert.showAndWait();
            }
        });
    }
}
