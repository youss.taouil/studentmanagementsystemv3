package be.kdg.java2.sms.view.implementation;

import be.kdg.java2.sms.service.StudentsService;
import be.kdg.java2.sms.service.models.Student;
import be.kdg.java2.sms.view.Presenter;
import be.kdg.java2.sms.view.StudentsView;
import javafx.collections.FXCollections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class StudentsPresenterImpl implements Presenter {
    private static final Logger L = Logger.getLogger(StudentsPresenterImpl.class.getName());
    private final StudentsView studentsView;
    private final StudentsService studentsService;

    @Autowired
    public StudentsPresenterImpl(StudentsView studentsView, StudentsService studentsService) {
        this.studentsView = studentsView;
        this.studentsService = studentsService;
        loadStudents();
        addEventHandlers();
    }

    private void addEventHandlers() {
        studentsView.getBtnSave().setOnAction(event->{
            Student student = new Student(studentsView.getTfName().getText(),
                    studentsView.getDpBirth().getValue(),Double.parseDouble(studentsView.getTfLength().getText()));
            studentsService.addStudent(student);
            loadStudents();
        });
    }

    private void loadStudents() {
        studentsView.getTvStudents().setItems(FXCollections.observableList(studentsService.getAllStudents()));
    }
}
