package be.kdg.java2.sms.view;

import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public interface LoginView {
    Button getBtnAdd();

    Button getBtnLogin();

    TextField getTfName();

    PasswordField getPfPassword();
}
