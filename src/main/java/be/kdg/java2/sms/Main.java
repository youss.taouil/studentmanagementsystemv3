package be.kdg.java2.sms;

import be.kdg.java2.sms.view.implementation.LoginViewImpl;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.logging.Logger;

public class Main extends Application {
    private static final Logger L = Logger.getLogger(Main.class.getName());

    private AnnotationConfigApplicationContext context;

    public static void main(String[] args) {
        L.info("Starting Student Management System on thread: " + Thread.currentThread().getName());
        launch(args);
    }

    @Override
    public void init() throws Exception {
        super.init();
        context = new AnnotationConfigApplicationContext();
        context.scan(Main.class.getPackageName());
        context.refresh();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        L.info("Running start methode on thread: " + Thread.currentThread().getName());
        primaryStage.setScene(context.getBean(Scene.class));
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        context.stop();
        super.stop();
    }
}
