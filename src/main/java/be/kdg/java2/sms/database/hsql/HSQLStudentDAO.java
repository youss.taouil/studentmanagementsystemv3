package be.kdg.java2.sms.database.hsql;

import be.kdg.java2.sms.database.DataSource;
import be.kdg.java2.sms.database.StudentDAO;
import be.kdg.java2.sms.exceptions.StudentException;
import be.kdg.java2.sms.service.models.Student;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class HSQLStudentDAO implements StudentDAO {
    private static final Logger L = Logger.getLogger(HSQLStudentDAO.class.getName());

    @Override
    public List<Student> retreiveAll() {
        try {
            PreparedStatement preparedStatement = DataSource.getInstance().getConnection().prepareStatement("SELECT * FROM STUDENTS");
            ResultSet rs = preparedStatement.executeQuery();
            List<Student> students = new ArrayList<>();
            while (rs.next()) {
                students.add(new Student(rs.getString("NAME"),
                        rs.getDate("BIRTHDAY").toLocalDate(), rs.getDouble("LENGTH")));
            }
            preparedStatement.close();
            return students;
        } catch (SQLException e) {
            L.warning("Problem retreiving all students from database:" + e.getMessage());
            throw new StudentException(e);
        }
    }

    @Override
    public void add(Student student) {
        try {
            PreparedStatement preparedStatement = DataSource.getInstance().getConnection().prepareStatement("INSERT INTO STUDENTS VALUES (NULL, ?, ?, ?)");
            preparedStatement.setString(1,student.getName());
            preparedStatement.setDate(2, Date.valueOf(student.getBirthday()));
            preparedStatement.setDouble(3,student.getLength());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            L.warning("Problem adding student to database:" + e.getMessage());
            throw new StudentException(e);
        }

    }
}
