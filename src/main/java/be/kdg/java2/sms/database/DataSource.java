package be.kdg.java2.sms.database;

import be.kdg.java2.sms.service.models.Student;

import java.sql.*;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class DataSource {
    private static final Logger L = Logger.getLogger(DataSource.class.getName());
    private static DataSource instance;
    private final Connection connection;

    public static DataSource getInstance() throws SQLException {
        if (instance==null) {
            instance = new DataSource();
        }
        return instance;
    }

    private DataSource() throws SQLException {
        try {
            connection = DriverManager.getConnection("jdbc:hsqldb:file:database/studentdb", "sa", "");
        } catch (SQLException exception) {
            L.warning("Problem connecting to database:" + exception.getMessage());
            throw exception;
        }
        try {
            createTables();
        } catch (SQLException exception) {
            L.warning("Problem creating database tables:" + exception.getMessage());
            throw exception;
        }
    }

    private void createTables() throws SQLException {
        L.info("Creating tables...");
        L.info("Check if allready there...");
        DatabaseMetaData dbm = connection.getMetaData();
        ResultSet tables = dbm.getTables(null, null, "USERS", null);
        if (!tables.next()) {
            L.info("Table USERS does not yet exists, creating it and all others");
            Statement statement = connection.createStatement();
            statement.execute("CREATE TABLE USERS (ID IDENTITY NOT NULL , NAME VARCHAR(100) NOT NULL, PASSWORD VARCHAR(20) NOT NULL )");
            statement.execute("CREATE TABLE STUDENTS (ID IDENTITY NOT NULL , NAME VARCHAR(100) NOT NULL, BIRTHDAY DATE, LENGTH DOUBLE)");
            //We add some intial data...
            List<Student> students = Arrays.asList(new Student("joske", LocalDate.of(1974,4,25),1.75),
                    new Student("jefke", LocalDate.of(1980,8,17),1.67),
                    new Student("mieke", LocalDate.of(1987,2,25),1.98));
            statement.close();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO STUDENTS VALUES (NULL, ?, ?, ?)");
            for (Student student : students) {
                preparedStatement.setString(1,student.getName());
                preparedStatement.setDate(2,Date.valueOf(student.getBirthday()));
                preparedStatement.setDouble(3,student.getLength());
                preparedStatement.executeUpdate();
            }
            preparedStatement.close();
        } else {
            L.info("Table USERS does already exist!");
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void close() throws SQLException {
        try (connection) {
            connection.close();
        } catch (SQLException exception) {
            L.warning("Problem closing connection to database:" + exception.getMessage());
            throw exception;
        }
    }
}
