package be.kdg.java2.sms.database;

import be.kdg.java2.sms.service.models.Student;

import java.util.List;

public interface StudentDAO {
    List<Student> retreiveAll();

    void add(Student student);
}
