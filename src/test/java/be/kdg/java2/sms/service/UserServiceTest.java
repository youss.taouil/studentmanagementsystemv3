package be.kdg.java2.sms.service;

import be.kdg.java2.sms.database.UserDAO;
import be.kdg.java2.sms.exceptions.StudentException;
import be.kdg.java2.sms.service.implementation.UserServiceImpl;
import be.kdg.java2.sms.service.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

class UserServiceTest {
    private UserService userService;

    class DummyUserDAO implements UserDAO {
        private Map<String, User> users = Map.of("duke", new User("duke", "java"));

        @Override
        public User getUserByName(String userName) {
            return users.get(userName);

        }        @Override
        public void addUser(User user) {
            //do nothing
        }
    }

    @BeforeEach
    public void setUp() throws Exception {
        userService = new UserServiceImpl(new DummyUserDAO());
    }

    @Test
    void addUser() {
        assertThrows(StudentException.class, () -> {
            userService.addUser("jos", "13");
        }, "Password not tested for strongness!");
    }

    @Test
    void login() {
        //TODO: write login tests
        assertFalse(userService.login("niemand", "ongeldig"),
          "Login van onbestaande gebruiker moet falen");
        assertFalse(userService.login("duke", "ellington"), "Login met ongeldig paswoord moet falen");
        assertTrue(userService.login("duke", "java"),
          "Login van gebruiker duke met wachtwoord java moet lukken");
        assertFalse(userService.login("Duke", "java"),
          "Login van gebruiker Duke (met hoofdletter) moet falen");
    }
}