package be.kdg.java2.sms.service;

import be.kdg.java2.sms.database.StudentDAO;
import be.kdg.java2.sms.service.implementation.StudentsServiceImpl;
import be.kdg.java2.sms.service.models.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

class StudentsServiceTest {
    private StudentsService studentsService;

    class DummyStudentDAO implements StudentDAO {

        @Override
        public List<Student> retreiveAll() {
            return null;//TODO
        }

        @Override
        public void add(Student student) {
            //TODO
        }
    }

    @BeforeEach
    public void setUp() throws Exception {
        studentsService = new StudentsServiceImpl(new DummyStudentDAO());
    }

    @Test
    void getAllStudents() {
        //TODO
    }

    @Test
    void addStudent() {
        //TODO
    }
}